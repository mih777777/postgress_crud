const bodyParser = require('body-parser')
const consola = require('consola')
const { Nuxt, Builder } = require('nuxt')


const { Client } = require('pg')
const express = require('express')
const app = express()

const client = new Client({
    user:'postgres',
    host:'localhost',
    database: 'app_base',
    password:'1',
    port:5432
})

client.connect()


app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

app.all('/*', (req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
})



app.post('/users/create', async (req,response) => {
  const query = {
    text: 'INSERT INTO users(name, age) VALUES($1, $2)',
    values: [req.body.name, req.body.age],
  }
  await client.query(query)
    
  response.redirect('/');
})



app.delete('/delete/:id', async (req,res) => {
  const id = parseInt(req.params.id)

  await client.query('DELETE FROM users WHERE id = $1', [id], (error, results) => {
  if (error) {
    throw error
  }
    res.status(200).send(`User deleted with ID: ${id}`)
  })   
})


app.put('/users/update/:id', async (req, res) => {
  
  const id = parseInt(req.params.id)
  const { name, age } = req.body

  await client.query(
    'UPDATE users SET name = $1, age = $2  WHERE id = $3',
    [name, age, id],
    (error, results) => {
      if (error) {
        throw error
      }
      res.status(200).send(`User modified with ID: ${id}`)
    }
  ) 
      
})



app.get('/users', async (req, res) => {
  await client.query('SELECT * FROM users ORDER BY id DESC', (error, result) => {
    if (error) {
      throw error
    }
    res.send(result.rows)
  })
})
  

app.get('/users/:id', async (req,response) => {
  const id = req.params.id
  await client.query('SELECT * from users WHERE id = $1', [id])
      .then(res => response.send(res.rows[0]))
      .catch(e => console.error(e.stack))
})


// Import and Set Nuxt.js options
let config = require('../nuxt.config.js')
config.dev = !(process.env.NODE_ENV === 'production')

async function start() {
  // Init Nuxt.js
  const nuxt = new Nuxt(config)

  const { host, port } = nuxt.options.server

  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  } else {
    await nuxt.ready()
  }

  // Give nuxt middleware to express
  app.use(nuxt.render)

  // Listen the server
  
    app.listen(port, host)
    consola.ready({
      message: `Server listening on http://${host}:${port}`,
      badge: true
    })
  
}
start()