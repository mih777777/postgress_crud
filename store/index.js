export const actions = {
    login({commit}) {
        commit('setToken', 'true')
    }
}

export const state = () => ({
    token: null
})

export const mutation = {
    setToken(state, token) {
        state.token = token
    }
}